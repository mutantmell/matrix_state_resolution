{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}

module StateResReloaded
where

import GHC.Generics (Generic)
import Control.Monad (guard)
import Data.Foldable (foldl', minimumBy)
import Data.Hashable
import qualified Data.HashMap.Strict as Map
import qualified Data.HashSet as Set
import qualified Data.List as List
import Data.Maybe (catMaybes, fromJust, fromMaybe, listToMaybe, mapMaybe)
import Data.List (find, foldl', foldl1', partition, sortBy, unfoldr)
import Data.Ord


data EventType -- enum of event types that we will use
  = Create
  | PowerLevels
  | JoinRules
  | Membership
  | Topic
  | Message
  deriving (Eq, Generic)
data Event -- the event data structure
  = Event { eventId :: String
          , eventType :: EventType
          , timeStamp :: Int
          , stateKey :: Maybe String
          , sender :: String
          , content :: String
          , prevEvents :: [Event]
          , authEvents :: [Event]
          , powerLevels :: Maybe (Map.HashMap String Int) -- only for PowerLevels events
          }
newStateEvent
  = Event { eventId = ""
          , eventType = Create
          , timeStamp = 0
          , stateKey = Just ""
          , sender = ""
          , content = ""
          , prevEvents = []
          , authEvents = []
          , powerLevels = Nothing
          }

newMessageEvent
  = Event { eventId = ""
          , eventType = Message
          , timeStamp = 0
          , stateKey = Nothing
          , sender = ""
          , content = ""
          , prevEvents = []
          , authEvents = []
          , powerLevels = Nothing
          }

join user
  = newStateEvent { eventType = Membership
                  , sender = user
                  , stateKey = Just user
                  , content = "join"
                  }

leave user
  = newStateEvent { eventType = Membership
                  , sender = user
                  , stateKey = Just user
                  , content = "leave"
                  }

u1 `invites` u2
  = newStateEvent { eventType = Membership
                  , sender = u1
                  , stateKey = Just u2
                  , content = "invite"
                  }

u1 `kicks` u2
  = newStateEvent { eventType = Membership
                  , sender = u1
                  , stateKey = Just u2
                  , content = "leave"
                  }

u1 `bans` u2
  = newStateEvent { eventType = Membership
                  , sender = u1
                  , stateKey = Just u2
                  , content = "ban"
                  }

user `setsPowerLevels` pl
  = newStateEvent { eventType = PowerLevels
                  , sender = user
                  , powerLevels = Just $ Map.fromList pl
                  }

user `setsTopic` topic
  = newStateEvent { eventType = Topic
                  , sender = user
                  , content = topic
                  }
type StateSet = Map.HashMap (EventType,String) Event

insertEvent :: Event -> StateSet -> StateSet
insertEvent
  e@(Event { eventType = eventType
           , stateKey = Just stateKey })
  stateSet
  = Map.insert (eventType, stateKey) e stateSet

-- construct a state set from a list of events
stateSetFromEventList :: [Event] -> StateSet
stateSetFromEventList
  = Map.fromList
    . map (\e@(Event { eventType = eventType
                     , stateKey = Just stateKey }) -> ((eventType, stateKey), e))
instance Show EventType where
  show Create = "Create"
  show PowerLevels = "Power Levels"
  show JoinRules = "Join Rules"
  show Membership = "Membership"
  show Topic = "Topic"
  show Message = "Message"
instance Eq Event where
  (==) e1 e2 = (eventId e1) == (eventId e2)

instance Show Event where
  show = show . eventId
instance Hashable EventType

instance Hashable Event where
  hashWithSalt i = hashWithSalt i . eventId

authChain :: Event -> (Set.HashSet Event)
authChain (Event { authEvents = e })
  -- ((Set.fromList e):(map authChain e)) means to prepend (Set.fromList e) to
  -- the list returned by (map authChain e), yielding a list of sets.
  -- Set.unions calculates the union of all these sets
  = Set.unions ((Set.fromList e):(map authChain e))
-- is the sender in the room?
isInRoom :: String -> StateSet -> Bool
isInRoom sender stateSet
  = maybe False (\x -> (content x) == "join") $ Map.lookup (Membership,sender) stateSet

-- get the power levels (if any) from the state set
stateSetPowerLevels :: StateSet -> Maybe (Map.HashMap String Int)
stateSetPowerLevels stateSet
  = Map.lookup (PowerLevels,"") stateSet
  >>= powerLevels

-- determine if the user has sufficient power level
hasPowerLevel :: String -> Maybe (Map.HashMap String Int) -> Int -> Bool
hasPowerLevel _ Nothing _ = True -- if the room has no power levels, then it's a free-for-all
hasPowerLevel sender (Just powerLevels) pl = (Map.lookupDefault 0 sender powerLevels) >= pl

isAuthorized :: Event -> StateSet -> Bool
-- create events are only allowed if they are the first event
isAuthorized
  (Event { eventType = Create
         , stateKey = Just _
         , prevEvents = prevEvents})
  stateSet
  = null prevEvents -- null tests whether it is empty
-- FIXME: join events
-- FIXME: power level events
-- everything else: PL 50 for state events, PL 0 for messages
isAuthorized
  (Event { stateKey = stateKey
         , sender = sender })
  stateSet
  = isInRoom sender stateSet
    && hasPowerLevel sender (stateSetPowerLevels stateSet) (if (stateKey == Nothing) then 0 else 50)
isAuthorized' :: Event -> StateSet -> Bool
isAuthorized' e stateSet = isAuthorized e augmentedStateSet
  where
    -- fold takes a function, initial accumulator value, and a list, and then
    -- iterates over the values in the list, calling the function to update the
    -- accumulator, and returns the final value
    augmentedStateSet = foldl' insertMissingState stateSet $ authEvents e
    insertMissingState stateSet e@(Event { eventType = eventType
                                         , stateKey = Just stateKey })
      = if (eventType, stateKey) `Map.member` stateSet
        then stateSet
        else Map.insert (eventType, stateKey) e stateSet
iterativeAuthChecks :: [Event] -> StateSet -> StateSet
iterativeAuthChecks events stateSet = foldl' addStateIfAuthorized stateSet events
  where
    addStateIfAuthorized stateSet e
      = if isAuthorized' e stateSet
        then insertEvent e stateSet
        else stateSet
isPowerEvent :: Event -> Bool
isPowerEvent (Event { eventType = PowerLevels
                    , stateKey = Just _ }) = True
isPowerEvent (Event { eventType = JoinRules
                    , stateKey = Just _ }) = True
isPowerEvent (Event { eventType = Membership
                    , sender = sender
                    , content = "ban"
                    , stateKey = Just stateKey }) = sender /= stateKey
-- a kick is a leave event where the sender of the event is not the same as the
-- person leaving (the state key)
isPowerEvent (Event { eventType = Membership
                    , sender = sender
                    , content = "leave"
                    , stateKey = Just stateKey }) = sender /= stateKey
isPowerEvent _ = False
calculateConflict :: [StateSet] -> (StateSet, (Set.HashSet Event))
calculateConflict stateSets = (unconflictedStateMap, conflictedStateMap)
  where
    domain = Set.fromList $ concat $ map Map.keys stateSets
    fullStateMapList = map (\k -> (k, eventsForKey k)) $ Set.toList domain
      where
        eventsForKey key = Set.fromList $ map (Map.lookup key) stateSets
    (unconflictedList, conflictedList) = partition (\(k, events) -> Set.size events == 1) fullStateMapList
    unconflictedStateMap
      -- convert single-element sets to just the element, and convert to map
      = Map.fromList
        $ map (\(k, eventSet)
                -> (k, fromJust $ head $ Set.toList eventSet)) unconflictedList
    conflictedStateMap
      -- collect all the non-Nothing elements, and convert to a set
      = Set.fromList $ catMaybes $ concat $ map (Set.toList . snd) conflictedList
fullAuthChain :: [Event] -> (Set.HashSet Event)
fullAuthChain stateSet = Set.unions $ map authChain stateSet

authDifference :: [StateSet] -> (Set.HashSet Event)
authDifference stateSets = authChainsUnion `Set.difference` authChainsIntersection
  where
    authChainsUnion = Set.unions fullAuthChains
    authChainsIntersection = foldl1' Set.intersection fullAuthChains
    fullAuthChains = map (fullAuthChain . Map.elems) stateSets
fullConflictedSet :: [StateSet] -> (Set.HashSet Event)
fullConflictedSet stateSets = conflictedSet `Set.union` (authDifference stateSets)
  where
    (_, conflictedSet) = calculateConflict stateSets

-- find the power level event (if any) in an e's auth events
_findPL :: Event -> Maybe Event
_findPL e = find (\x -> eventType x == PowerLevels) $ authEvents e

-- order by # of items in auth chain
data EventPriority
  = EventPriority { vertexAuthChain :: Set.HashSet Event
                  , revAuthChain :: Set.HashSet Event
                  , remainingOutdegree :: Int
                  , senderPowerLevel :: Int
                  }

revTopPowSort :: [Event] -> [Event]
revTopPowSort events = kahn priority graph
  where
    -- This allows us to chain together comparisons, falling back to
    -- other comparisons when earlier ones are equal.
    a <&&> b = \x y -> a x y <> b x y
    power e = _findPL e >>= powerLevels >>= Map.lookup 0 (sender e)
    priority = comparing power
             <&&> comparing timeStamp
             <&&> comparing eventId
    label = eventId
    childrenOf event = eventId <$> prevEvents event
    graph = makeGraph label childrenOf events

-- The mainline list of an event e.  unfoldr generates a list by iteratively
-- applying a function until it returns Nothing.  The function returns a tuple
-- where the first element is the element to add to the list and the second
-- element is the input to the next iteration.
mainline e = unfoldr (maybe Nothing (\event -> Just (event, _findPL event))) (Just e)

mainlineOrder :: Event -> Event -> Event -> Ordering
mainlineOrder p e1 e2
  = comparing mainlineDepth e1 e2
    ||| comparing timeStamp e1 e2
    ||| comparing eventId e1 e2
  where
    -- define a new operator that allows us to chain comparisons and return the
    -- first comparison that is not equality
    LT ||| _ = LT
    GT ||| _ = GT
    EQ ||| val = val

    -- creates a map from event to the negation of its position in the mainline
    -- list
    mainlineMap = Map.fromList $ zip (mainline p) [0,-1..]
    -- the "closest mainline event" of e is the first event in (mainline e)
    -- that is also in (mainline p).  So iterate through the mainline e list,
    -- look up each event in mainlineMap, and return the first non-Nothing
    -- result that we find.  If there is no non-Nothing result, then return
    -- Nothing (which will sort below everything else).
    mainlineDepth e = listToMaybe $ mapMaybe (\e -> Map.lookup e mainlineMap) $ mainline e
resolve :: [StateSet] -> StateSet
resolve stateSets
  = let
      (unconflictedStateMap, conflictedSet) = calculateConflict stateSets
      fullConflictedSet = conflictedSet `Set.union` (authDifference stateSets)
      conflictedPowerEvents = Set.filter isPowerEvent fullConflictedSet
      conflictedPowerEventsWithAuth
        = conflictedPowerEvents
          `Set.union`
          ((fullAuthChain $ Set.toList conflictedPowerEvents)
            `Set.intersection`
            fullConflictedSet)
      sortedPowerEvents = revTopPowSort $ Set.toList conflictedPowerEventsWithAuth
      partialResolvedState = iterativeAuthChecks sortedPowerEvents unconflictedStateMap
      otherConflictedEvents = fullConflictedSet `Set.difference` conflictedPowerEventsWithAuth
      resolvedPowerLevels = fromJust $ Map.lookup (PowerLevels, "") partialResolvedState
      sortedOtherEvents = sortBy (mainlineOrder resolvedPowerLevels) $ Set.toList otherConflictedEvents
      nearlyFinalState = iterativeAuthChecks sortedOtherEvents partialResolvedState
    -- Map.union will take the value from the first map if a key exists in both
    -- maps.
    in unconflictedStateMap `Map.union` nearlyFinalState
eventsToDot :: [Event] -> Set.HashSet Event -> IO ()
eventsToDot [] _ = return ()
eventsToDot (x:xs) omit = do
  printEdges x (prevEvents x) Set.empty ""
  printEdges x (authEvents x) omit " [color=red]"
  eventsToDot xs omit
  where
    printEdges _ [] _ _ = return ()
    printEdges e (x:xs) omit suffix
      | x `Set.member` omit = printEdges e xs omit suffix
      | otherwise = do
          putStr $ show e
          putStr " -> "
          putStr $ show x
          putStrLn suffix
          printEdges e xs omit suffix

-- Kahn Implementation

data Node a =
  Node { label :: String
       , entry :: a
       } deriving (Eq, Show, Generic)

data Edge =
  Edge { from :: String
       , to :: String
       } deriving (Eq, Show, Generic)

data Graph a =
  Graph { entries :: Map.HashMap String a
        , outbound :: Map.HashMap String (Set.HashSet String)
        , inbound :: Map.HashMap String (Set.HashSet String)
        } deriving (Eq, Show, Generic)

node :: String -> Graph a -> Maybe (Node a)
node lbl Graph{..} = Node lbl <$> Map.lookup lbl entries

nodes :: Graph a -> [Node a]
nodes = map (uncurry Node) . Map.toList . entries

isTop :: Node a -> Graph a -> Bool
isTop (Node label _) Graph{inbound} = not $ Map.member label inbound

tops :: Graph a -> [Node a]
tops g = filter (`isTop` g) (nodes g)

empty :: Graph a
empty = Graph Map.empty Map.empty Map.empty

makeGraph :: (Foldable f)
          => (a -> String)
          -> (a -> [String])
          -> f a
          -> Graph a
makeGraph mkLabel mkOutbound = foldl' f empty
  where
    f Graph{..} next = Graph entries' outbound' inbound'
      where
        nextLabel = mkLabel next
        nextOutbound = Set.fromList $ mkOutbound next
        entries' = Map.insert nextLabel next entries
        outbound' = if Set.null nextOutbound
          then outbound
          else Map.insert nextLabel nextOutbound outbound
        addInbound = maybe (Just $ Set.singleton nextLabel) (Just . Set.insert nextLabel)
        inbound' = Set.foldl' (flip (Map.alter addInbound)) inbound nextOutbound

prune :: Edge -> Graph a -> Graph a
prune Edge{from,to} Graph{..} = Graph entries outbound' inbound'
  where
    fromOut = Map.lookupDefault Set.empty from outbound
    toIn = Map.lookupDefault Set.empty to inbound
    (fromOut', toIn') = if Set.member to fromOut
      then (Set.delete to fromOut, Set.delete from toIn)
      else (fromOut, toIn)
    outbound' = if Set.null fromOut'
      then Map.delete from outbound
      else Map.insert from fromOut' outbound
    inbound' = if Set.null toIn'
      then Map.delete to inbound
      else Map.insert to toIn' inbound

pruneFrom :: Node a -> Graph a -> Maybe (Graph a, [Node a])
pruneFrom from g = run <$> toNodes
  where
    toNodes = traverse (`node` g)
            . Set.toList
            . Map.lookupDefault Set.empty (label from)
            $ outbound g
    run tos = go g tos []

    go g [] acc = (g, acc)
    go g (to:xs) acc = go g' xs acc'
      where
        g' = prune (Edge (label from) (label to)) g
        acc' = if isTop to g' then to:acc else acc

kahn :: (a -> a -> Ordering) -> Graph a -> Maybe [a]
kahn ordering g = go g sortedTops []
  where
    ordering' a b = ordering (entry a) (entry b)

    sortedTops = List.sortBy ordering' $ tops g

    validate Graph{outbound} = Map.null outbound

    insert = List.insertBy ordering'
    insertAll xs ys = foldr insert ys xs

    go g [] acc = reverse acc <$ guard (validate g)
    go g (x:xs) acc = do
      (g', newTops) <- pruneFrom x g
      go g' (insertAll newTops xs) (entry x:acc)  
